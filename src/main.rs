use std::{env, error::Error};

use futures::future::join_all;
use nanorand::{Rng, WyRand};
use reqwest::{
    multipart::{Form, Part},
    Body,
};
use serde::Serialize;
use serde_json::Value;
use serenity::{
    all::{Attachment, Message},
    async_trait,
    prelude::*,
};

struct Handler;
#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if !msg.author.bot {
            let mut rng = WyRand::new();
            if rng.generate_range(0..100) == 0 {
                if let Err(why) = msg.react(&ctx.http, '🙂').await {
                    println!("couldnt react: {why:?}");
                }
                if let Err(why) = post(msg.content_safe(&ctx.cache), msg.attachments).await {
                    println!("nao posto: {why:?}");
                }
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let token = env::var("DISCORD_TOKEN").expect("no token lol :p");
    let intents = GatewayIntents::non_privileged()
        | GatewayIntents::MESSAGE_CONTENT
        | GatewayIntents::GUILD_MESSAGES;
    let mut client = Client::builder(token, intents)
        .event_handler(Handler)
        .await
        .expect("oops...");

    if let Err(why) = client.start().await {
        println!("oops: {:?}", why);
    }
}

async fn post(mut message: String, attachments: Vec<Attachment>) -> Result<(), Box<dyn Error>> {
    let client = reqwest::Client::new();

    // upload files:
    let mut futures = Vec::new();
    for attachment in attachments {
        futures.push(file_upload(attachment, client.clone()))
    }
    let mut form = Form::new();
    for file in join_all(futures).await {
        match file {
            Ok(id) => form = form.text("media_ids[]", id),
            Err(why) => println!("{why:?}"),
        }
    }

    // format message:
    message.truncate(491);
    let formatted_message = format!("{message} 🙂 benas");
    form = form.text("status", formatted_message);

    //post
    client
        .post("https://botsin.space/api/v1/statuses")
        .header("Authorization", env::var("MASTODON_TOKEN")?)
        .multipart(form)
        .send()
        .await?
        .error_for_status()?;
    Ok(())
}

#[derive(Serialize)]
struct TwitterPost {
    text: String,
}

async fn file_upload(
    attachment: Attachment,
    client: reqwest::Client,
) -> Result<String, Box<dyn Send + Sync + Error>> {
    let discord_stream = client.get(attachment.url).send().await?.bytes_stream();
    let file_body = Body::wrap_stream(discord_stream);

    let file = Part::stream(file_body)
        .file_name(attachment.filename.clone())
        .mime_str(&attachment.content_type.unwrap());

    let form = Form::new().part("file", file?);
    let response: Value = client
        .post("https://botsin.space/api/v2/media")
        .header("Authorization", env::var("MASTODON_TOKEN")?)
        .multipart(form)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await?;

    Ok(response["id"].as_str().unwrap().to_string())
}
